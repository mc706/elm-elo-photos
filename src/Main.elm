module Main exposing (..)

import Browser
import Dict exposing (Dict)
import File exposing (File)
import File.Select as Select
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Keyboard exposing (Key(..), navigationKey)
import List.Extra
import Random
import Random.List
import Task exposing (Task)



---- MODEL ----


type Page
    = Rankings
    | Selection


type alias Image =
    { name : String
    , url : String
    }


type alias ScoreHistory =
    { wins : Int
    , rounds : Int
    , elo : Float
    }


emptyScore : ScoreHistory
emptyScore =
    { wins = 0
    , rounds = 0
    , elo = 1400.0
    }


type alias Model =
    { images : List Image
    , scores : Dict String ScoreHistory
    , round : List ( Image, Image )
    , hover : Bool
    , page : Page
    }


init : ( Model, Cmd Msg )
init =
    ( { images = []
      , scores = Dict.empty
      , round = []
      , hover = False
      , page = Rankings
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = Pick
    | DragEnter
    | DragLeave
    | GotFiles File (List File)
    | GotPreviews (List Image)
    | NewRound (List Image)
    | Choose Image Image
    | GoToRanks
    | GoToPicker
    | KeyPressed Keyboard.RawKey


fileToImage : File -> Task x Image
fileToImage file =
    let
        name =
            File.name file
    in
    File.toUrl file |> Task.andThen (\url -> Task.succeed <| Image name url)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Pick ->
            ( model
            , Select.files [ "image/*" ] GotFiles
            )

        DragEnter ->
            ( { model | hover = True }
            , Cmd.none
            )

        DragLeave ->
            ( { model | hover = False }
            , Cmd.none
            )

        GotFiles file files ->
            ( { model | hover = False }
            , Task.perform GotPreviews <|
                Task.sequence <|
                    List.map fileToImage (file :: files)
            )

        GotPreviews images ->
            ( { model | images = images }
            , Cmd.none
            )

        Choose winner loser ->
            let
                winnerOldScore =
                    Dict.get winner.name model.scores |> Maybe.withDefault emptyScore

                loserOldScore =
                    Dict.get loser.name model.scores |> Maybe.withDefault emptyScore

                ( winnerNewElo, loserNewElo ) =
                    calculateNewElos winnerOldScore.elo loserOldScore.elo

                winnerNewScore =
                    { wins = winnerOldScore.wins + 1, rounds = winnerOldScore.rounds + 1, elo = winnerNewElo }

                loserNewScore =
                    { wins = winnerOldScore.wins, rounds = winnerOldScore.rounds + 1, elo = loserNewElo }

                newScores =
                    model.scores
                        |> Dict.insert winner.name winnerNewScore
                        |> Dict.insert loser.name loserNewScore

                newModel =
                    { model
                        | round = List.drop 1 model.round
                        , scores = newScores
                    }
            in
            ( newModel
            , if newModel.round == [] then
                shuffleImages model.images

              else
                Cmd.none
            )

        NewRound round ->
            ( { model | round = List.map (toTuple (Image "" "")) <| List.Extra.groupsOf 2 round }, Cmd.none )

        GoToRanks ->
            ( { model | page = Rankings }, Cmd.none )

        GoToPicker ->
            ( { model | page = Selection }
            , if model.round == [] then
                shuffleImages model.images

              else
                Cmd.none
            )

        KeyPressed key ->
            case ( model.page, model.round ) of
                ( Selection, ( left, right ) :: _ ) ->
                    case navigationKey key of
                        Just ArrowLeft ->
                            ( model, send (Choose left right) )

                        Just ArrowRight ->
                            ( model, send (Choose right left) )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )


send : msg -> Cmd msg
send msg =
    Task.succeed msg
        |> Task.perform identity


calculateNewElos : Float -> Float -> ( Float, Float )
calculateNewElos winner loser =
    let
        k =
            32

        winnerExpected =
            1.0 / (1.0 + 10.0 ^ ((winner - loser) / 400.0))

        loserExpected =
            1.0 / (1.0 + 10.0 ^ ((loser - winner) / 400.0))

        newWinner =
            winner + (k * (1.0 - winnerExpected))

        newLoser =
            loser + (k * (0.0 - loserExpected))
    in
    ( newWinner, newLoser )


toTuple : a -> List a -> ( a, a )
toTuple default list =
    case list of
        a :: b :: _ ->
            ( a, b )

        a :: _ ->
            ( a, a )

        [] ->
            ( default, default )


shuffleImages images =
    Random.generate NewRound <| (Random.List.shuffle >> Random.andThen Random.List.shuffle) images



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    { title = "Elo Compare"
    , body = [ viewApp model ]
    }


viewApp : Model -> Html Msg
viewApp model =
    div [ class "main-app" ]
        [ div [ class "header" ]
            [ h3 [ class "title", style "margin-left" "25px" ] [ text "Image Ranker" ]
            , div [ classList [ ( "hidden", model.images == [] ) ] ]
                [ button [ onClick GoToRanks ] [ text "Ranks" ]
                , button [ onClick GoToPicker ] [ text "Picker" ]
                ]
            ]
        , div [ class "main-content" ]
            [ viewContent model ]
        ]


viewContent : Model -> Html Msg
viewContent model =
    case model.images of
        [] ->
            viewPicker model

        _ ->
            viewRankingApp model


viewRankingApp : Model -> Html Msg
viewRankingApp model =
    case model.page of
        Rankings ->
            viewRankingPage model

        Selection ->
            viewSelectionPage model


showImage : { a | url : String } -> Int -> Int -> Html Msg
showImage image width height =
    div
        [ style "width" <| String.fromInt width ++ "px"
        , style "height" <| String.fromInt height ++ "px"
        , style "background-image" ("url('" ++ image.url ++ "')")
        , style "background-position" "center"
        , style "background-repeat" "no-repeat"
        , style "background-size" "contain"
        ]
        []


viewRankingPage : Model -> Html Msg
viewRankingPage model =
    let
        images_with_ranking =
            model.images
                |> List.map (\i -> { name = i.name, url = i.url, score = Dict.get i.name model.scores |> Maybe.withDefault emptyScore })
                |> List.sortBy (.score >> .elo)
                |> List.reverse

        showRow row =
            tr []
                [ td [] [ showImage row 100 100 ]
                , td [] [ text row.name ]
                , td [] [ text <| String.fromFloat row.score.elo ]
                , td [] [ text <| String.fromInt row.score.wins ]
                , td [] [ text <| String.fromInt row.score.rounds ]
                , td [] [ text <| String.fromFloat <| (*) 100 <| toFloat row.score.wins / toFloat row.score.rounds ]
                ]
    in
    div [ class "rankings-page" ]
        [ h1 [] [ text "Image Rankings" ]
        , table [ class "table" ]
            [ thead []
                [ tr []
                    [ th [] [ text "Photo" ]
                    , th [] [ text "Name" ]
                    , th [] [ text "ELO" ]
                    , th [] [ text "Wins" ]
                    , th [] [ text "Rounds" ]
                    , th [] [ text "Win %" ]
                    ]
                ]
            , tbody [] (images_with_ranking |> List.map showRow)
            ]
        ]


viewSelectionPage : Model -> Html Msg
viewSelectionPage model =
    case model.round of
        [] ->
            div [] [ text "Loading new Round" ]

        ( left, right ) :: _ ->
            div [ class "selection-page" ]
                [ h1 [] [ text "Pick Your Favored Image" ]
                , div [] [ text "You can use the left and right arrow keys to quick pick" ]
                , div [ class "choices" ]
                    [ div [ class "picker", onClick (Choose left right) ] [ button [] [ text "Select" ], showImage left 700 700 ]
                    , div [ class "picker", onClick (Choose right left) ] [ button [] [ text "Select" ], showImage right 700 700 ]
                    ]
                ]


viewPicker : Model -> Html Msg
viewPicker model =
    div
        [ style "border"
            (if model.hover then
                "6px dashed purple"

             else
                "6px dashed #ccc"
            )
        , style "border-radius" "20px"
        , style "width" "480px"
        , style "margin" "100px auto"
        , style "padding" "40px"
        , style "display" "flex"
        , style "flex-direction" "column"
        , style "justify-content" "center"
        , style "align-items" "center"
        , hijackOn "dragenter" (D.succeed DragEnter)
        , hijackOn "dragover" (D.succeed DragEnter)
        , hijackOn "dragleave" (D.succeed DragLeave)
        , hijackOn "drop" dropDecoder
        ]
        [ button [ onClick Pick ] [ text "Upload Images" ]
        ]



---- DECODERS ---


dropDecoder : D.Decoder Msg
dropDecoder =
    D.at [ "dataTransfer", "files" ] (D.oneOrMore GotFiles File.decoder)


hijackOn : String -> D.Decoder msg -> Attribute msg
hijackOn event decoder =
    preventDefaultOn event (D.map hijack decoder)


hijack : msg -> ( msg, Bool )
hijack msg =
    ( msg, True )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Keyboard.ups KeyPressed
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.document
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }

# Elm Elo Compare

This app uses the elo ranking system to rank images uploaded to the browser. 

Inspired by the Social Network movie scene where Mark creates an app to rank 
classmates by hotness, comparing two and picking one and ranking accordingly. This
is mean to use to take a set of photos and decide which ones are best. 

This only works locally, on files you upload, and no data is sent serverside 
anywhere. 

Written in Elm. 
